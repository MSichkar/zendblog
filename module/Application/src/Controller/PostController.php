<?php
namespace Application\Controller;


use Application\Form\PostForm;
use Zend\Mvc\Controller\AbstractActionController;
use Doctrine\ORM\EntityManager;
use Application\Service\PostManager;
use Zend\View\Model\ViewModel;
use Application\Entity\Post;
class PostController extends AbstractActionController
{
    /**
     * Менеджер сущностей.
     * @var EntityManager
     */
    private $entityManager;

    /**
     * Менеджер постов.
     * @var PostManager
     */
    private $postManager;

    /**
     * Конструктор, используемый для внедрения зависимостей в контроллер.
     */
    public function __construct($entityManager, $postManager)
    {
        $this->entityManager = $entityManager;
        $this->postManager = $postManager;
    }

    /**
     * Это действие отображает страницу "New Post". Она содержит
     * форму, позволяющую ввести заголовок поста, содержимое и теги.
     * Когда пользователь нажимает кнопку отправки формы, создается
     * новая сущность Post.
     */
    public function addAction()
    {
        // Создаем форму.
        $form = new PostForm();

        // Проверяем, является ли пост POST-запросом.
        if ($this->getRequest()->isPost()) {

            // Получаем POST-данные.
            $data = $this->params()->fromPost();

            // Заполняем форму данными.
            $form->setData($data);
            if ($form->isValid()) {

                // Получаем валидированные данные формы.
                $data = $form->getData();

                // Используем менеджер постов для добавления нового поста в базу данных.
                $this->postManager->addNewPost($data);

                // Перенаправляем пользователя на страницу "index".
                return $this->redirect()->toRoute('application');
            }
        }

        // Визуализируем шаблон представления.
        return new ViewModel([
            'form' => $form
        ]);
    }

    public function editAction()
    {
        // Создаем форму.
        $form = new PostForm();

        // Получаем ID поста.
        $postId = $this->params()->fromRoute('id', -1);

        // Находим существующий пост в базе данных.
        $post = $this->entityManager->getRepository(Post::class)
            ->findOneById($postId);
        if ($post == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        // Проверяем, является ли пост POST-запросом.
        if ($this->getRequest()->isPost()) {

            // Получаем POST-данные.
            $data = $this->params()->fromPost();

            // Заполняем форму данными.
            $form->setData($data);
            if ($form->isValid()) {

                // Получаем валидированные данные формы.
                $data = $form->getData();

                // Используем менеджер постов, чтобы добавить новый пост в базу данных.
                $this->postManager->updatePost($post, $data);

                // Перенаправляем пользователя на страницу "admin".
                return $this->redirect()->toRoute('posts', ['action'=>'admin']);
            }
        } else {
            $data = [
                'title' => $post->getTitle(),
                'content' => $post->getContent(),
                'tags' => $this->postManager->convertTagsToString($post),
                'status' => $post->getStatus()
            ];

            $form->setData($data);
        }

        // Визуализируем шаблон представления.
        return new ViewModel([
            'form' => $form,
            'post' => $post
        ]);
    }

    public function viewAction()
    {
        $postId = $this->params()->fromRoute('id', -1);

        $post = $this->entityManager->getRepository(Post::class)
            ->findOneById($postId);

        if ($post == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $commentCount = $this->postManager->getCommentCountStr($post);

        // Создаем форму.
        $form = new CommentForm();

        // Проверяем, является ли пост POST-запросом.
        if($this->getRequest()->isPost()) {

            // Получаем POST-данные.
            $data = $this->params()->fromPost();

            // Заполняем форму данными.
            $form->setData($data);
            if($form->isValid()) {

                // Получаем валадированные данные формы.
                $data = $form->getData();

                // Используем менеджер постов для добавления нового комментарий к посту.
                $this->postManager->addCommentToPost($post, $data);

                // Снова перенаправляем пользователя на страницу "view".
                return $this->redirect()->toRoute('posts', ['action'=>'view', 'id'=>$postId]);
            }
        }

        // Визуализируем шаблон представления.
        return new ViewModel([
            'post' => $post,
            'commentCount' => $commentCount,
            'form' => $form,
            'postManager' => $this->postManager
        ]);
    }
    public function deleteAction()
    {
        $postId = $this->params()->fromRoute('id', -1);

        $post = $this->entityManager->getRepository(Post::class)
            ->findOneById($postId);
        if ($post == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $this->postManager->removePost($post);

        // Перенаправляем пользователя на страницу "index".
        return $this->redirect()->toRoute('posts', ['action'=>'admin']);
    }

    public function adminAction()
    {
        // Get recent posts
        $posts = $this->entityManager->getRepository(Post::class)
            ->findBy([], ['dateCreated'=>'DESC']);

        // Render the view template
        return new ViewModel([
            'posts' => $posts,
            'postManager' => $this->postManager
        ]);
    }

}